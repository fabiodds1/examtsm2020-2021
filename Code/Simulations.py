import statistics
from matplotlib import pyplot as plt
import numpy as np
import random
from Simulationfunctions import defense, aggress


class Simulation:
    def aggressive(self, amt_lst):
        """
        This function calculates the mean of final values of 500 aggressive investors for each year
        :param amt_lst: initial captial amount
        :return: list of mean of 500 final amounts of each year
        """
        def_lst_mean = []  # list to append mean of all 500 investors for each year
        amt_lst_yr = [[], [], [], [], []]  # list of lists to append final value of each year for 500 investors which will be helpful to calculate mean for each year
        year_ls = [2016, 2017, 2018, 2019, 2020]
        i = 0
        for year in year_ls:  # loop for each year in the list of years
            for y in range(500):  # loop for 500 investors
                """
                - if else loop check for each year appends the returned final value accordingly
                - It calls the agress function from Simulationfunctions files to perform the operations and calculate final value
                - All the final values of each year is stored in amt_lst_yr sublist to calculate yearly mean 
                """
                if year == year_ls[0]:
                    total = aggress(amt_lst, year)
                    amt_lst_yr[i].append(total)  # for first year all the final values are stored in amt_lst_year[0]
                else:
                    """# Here the list accesses the previous year's value
                    of investor y and send it in the function to perform the aggressive operation for the given year"""
                    total = aggress(amt_lst_yr[i - 1][y], year)

                    """# Here the list appends the current year's final value of all the investors """
                    amt_lst_yr[i].append(total)
            i = i + 1

        for j in amt_lst_yr:  # loop to calculate yearly mean with the help of already stored final value in amt_lst_year
            mean_val = statistics.mean(j)
            def_lst_mean.append(round(mean_val, 2))
        return def_lst_mean

    def defensive(self, amt_lst):
        """
        This function calculates the mean of final values of 500 defensive investors for each year
        :param amt_lst: initial captial amount
        :return: list of mean of 500 final amounts of each year
        """
        amt_lst_yr = [[], [], [], [], []]  # list of lists to append final value of each year for 500 investors which will be helpful to calculate mean for each year
        def_lst_mean = []  # list to append mean of all 500 investors for each year
        for i in range(5):
            for y in range(500):
                if i == 0:
                    total = defense(amt_lst)
                    amt_lst_yr[i].append(total)  # for first year all the final values are stored in amt_lst_year[0]
                else:
                    """ Here the list accesses the previous year's value
                    of investor y and send it in the function to perform the defensive operation for the given year"""
                    total = defense(amt_lst_yr[i - 1][y])

                    """# Here the list appends the current year's final value of all the investors """
                    amt_lst_yr[i].append(total)

        for j in amt_lst_yr:  # loop to calculate yearly mean with the help of already stored final value in amt_lst_year
            mean_val = statistics.mean(j)
            def_lst_mean.append(round(mean_val, 2))
        return def_lst_mean


class Mix:
    def mixed(self,amt_lst,b, s):
        """

        :param amt_lst: initial captial amount
        :param b: Percentage to select bond amount
        :param s: Percentage to select stock amount
        :return: list of mean of 500 final amounts of each year
        """
        bond = b * amt_lst  # bond amount
        stock = s * amt_lst  # stock amount
        sim = Simulation()

        """ call aggressive function of simulation class and pass stock amount as amount, which returns mean list of 
        500 final amounts of each year """
        agg = sim.aggressive(stock)

        """ call defensive function of simulation class and pass bond amount as amount, which returns mean list of 500 
        final amounts of each year """
        defen = sim.defensive(bond)

        # add the bond qnd stock amounts of each year to calculate mean final value of each year
        mix_mean = np.add(agg, defen)
        return mix_mean.tolist()


"""
Start of the execution of Simulations
"""

amount1 = random.randint(5000,15000) # initial capital randomly selected
d = Simulation()
ls_agg = d.aggressive(amount1)
ls_def = d.defensive(amount1)

mix_val = Mix()
ls_mix1 = mix_val.mixed(amount1,0.25,0.75)

"""redistributing the final amount of mixed investor by passing the first year's mean final value and changing the bond 
and stock percentage """
ls_mix2 = mix_val.mixed(ls_mix1[0],0.75,0.25)

profit_final_agg = []
profit_final_def = []
profit_final_mix1 = []
profit_final_mix2 = []

for x in ls_agg:  # loop to append the mean profit of aggressive investors into a list
    profit_final_agg.append(x-amount1)
print("For Aggressive")
print("profit mean")
print(profit_final_agg)
print("final value mean")
print(ls_agg)

for x in ls_def:  # loop to append the mean profit of defensive investors into a list
    profit_final_def.append(x-amount1)
print("For Defensive")
print("profit mean")
print(profit_final_def)
print("final value mean")
print(ls_def)

for x in ls_mix1:  # loop to append the mean profit of mixed investors into a list
    profit_final_mix1.append(x-amount1)
print("For Mix")
print("profit mean")
print(profit_final_mix1)
print("final value mean")
print(ls_mix1)

for x in ls_mix2:  # loop to append the mean profit of mixed investors into a list after redistribution
    profit_final_mix2.append(x-amount1)
print("For Mix after redistribution")
print("profit mean")
print(profit_final_mix2)
print("final value mean")
print(ls_mix2)

plt.figure(figsize=[15, 10])
# Data to be plotted

# Group four different data Aggressive, Defensive, Mixed and Mixed redistribute with bars
Fig = np.arange(len(ls_agg))
# Create bar plot and using X now to align the bars side by side
plt.bar(Fig, ls_agg, color = 'm', width = 0.25)
plt.bar(Fig + 0.2, ls_def, color = 'orange', width = 0.2)
plt.bar(Fig + 0.4, ls_mix1, color = 'r', width = 0.2)
plt.bar(Fig + 0.6, ls_mix2, color = 'blue', width = 0.2)

# Defining the legend
plt.legend(['Aggresive', 'Defensive', 'Mixed1','Mixed2'])
# Years title as x-axis ticks
plt.xticks([i + 0.25 for i in range(5)], ['2016', '2017', '2018', '2019', '2020'])
# Defining tilte for the plot
plt.title("Investment performance from 2016-2020 ")
# Label the x and y axis
plt.xlabel('Years')
plt.ylabel('Return in Dollars')
# Save plot
plt.savefig('BarPlot1.png')
# Display graph plot
plt.show()
