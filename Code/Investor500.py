from Simulationfunctions import random_date
from Part1 import short, long
import random
import datetime as dt
import functions


def defensive(amount2):  # Defensive investor
    n = random.randint(2, 10)
    a = 0.5 * amount2
    c = 250
    div = a % c
    d = a - div

    h = short(d, n)  # calling the subclass corresponding to short-term bond from part1 and creating a new object h
    FVS, compound_value_s = h.compute_price()
    print("The future value is " + str(round(FVS, 2)) + " and the compound interest is " + str(
        round(compound_value_s, 2)))

    t = long(d, n)  # calling the subclass corresponding to long-term bond from part1 and creating a new object h
    FVL, compound_value_l = t.compute_price()
    print("The future value is " + str(round(FVL, 2)) + " and the compound interest is " + str(
        round(compound_value_l, 2)))


def aggressive(amount3):

    start_dt = dt.datetime(2016, 1, 9)
    end_dt = dt.datetime(2021, 1, 1)
    start_date = random_date(start_dt, end_dt)
    print(start_date)

    lst_stock = ['FDX', 'GOOGL', 'XOM', 'KO', 'NOK', 'MS', 'IBM']  # List of all the stocks
    stock_name = random.choice(lst_stock)
    print("First investment stock is " + stock_name)

    initial_price, no_stocks = functions.stock_details(stock_name, start_date, amount3)
    print("Total number of stocks is " + str(no_stocks))

    remaining_amount = amount3 % initial_price
    print("The remaining amount after buying first stock: " + str(round(remaining_amount, 2)))

    lst_stock2 = lst_stock
    lst_stock2.remove(stock_name)
    print(lst_stock2)

    initial_price_new = 0

    while initial_price_new < remaining_amount:
        if lst_stock2:
            stock_namenew = random.choice(lst_stock2)
            initial_price_new, no_stocks1 = functions.stock_details(stock_namenew, start_date, remaining_amount)
            lst_stock2.remove(stock_namenew)
            if no_stocks1 == 0:
                initial_price_new = 0
            else:
                remaining_amount = remaining_amount % initial_price_new
                print("Total number of stocks is " + str(no_stocks1))
                print("The remaining amount after buying new stock " + str(round(remaining_amount, 2)))
        else:
            print("Not sufficient money to buy another stock")
        break


def mixed(amount1):

    bond = 0.25 * amount1
    stock = 0.75 * amount1
    defensive(bond)
    aggressive(stock)


for x in range(500):
    amount = random.randint(5000, 10000)
    defensive(amount)
    aggressive(amount)
    mixed(amount)
