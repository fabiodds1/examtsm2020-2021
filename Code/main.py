from Part1 import short, long
from matplotlib import pyplot as plt

if __name__ == '__main__':
    print("Short term bonds have a minimum term of 2  years, a minimum amount of 250$ and a yearly interest of 1.5% \n", \
          "Long term bonds have a minimum term of 5  years, a minimum amount of 1000$ and a yearly interest of 3% ")
    print("\n")

    # Input from user the price of the bond and term of his investment
    p = int(input("Please enter the bond price. "))  # assuming bonds are purchased at par
    n = int(input("Please enter the term of your investment in years. "))

    # checking the minimum years and the investment required to compute the future values
    if (n >= 2) & (n < 5) & (p >= 250):
        h = short(p, n)  # calling the subclass corresponding to short-term bond from part1 and creating a new object h
        FV, compound_value = h.compute_price()
        print("The future value is " + str(round(FV, 2)) + " and the compound interest is " + str(
            round(compound_value, 2)))
    elif (n >= 5) & (p >= 1000):
        h = long(p, n)  # calling the subclass corresponding to long-term bond from part1 and creating a new object h
        FV, compound_value = h.compute_price()
        print("The future value is " + str(round(FV, 2)) + " and the compound interest is " + str(
            round(compound_value, 2)))
    else:
        print("Minimum term is 2 years or price greater than 250")

    """Creating lists displaying the evolution of both bond types with minimum amount invested from year 0 to year
    50 years"""
    print('The graph plot is not related to the inputs provided by the user')
    print('The minimum amount and 50 years are used to plot the graph')
    FV_short_list = []  # list to store 50 years short term bond values
    FV_long_list = []  # list to store 50 years long term bond values

    # loop to compute future values for short and long term bond for 50 years and store it in list
    for x in range(50):
        FV_short = 250 * ((1 + 0.015) ** (x))
        FV_short_list.append(round(FV_short, 2))
        FV_long = 1000 * ((1 + 0.03) ** (x))
        FV_long_list.append(round(FV_long, 2))
    print(FV_short_list)
    print(FV_long_list)

    # Plotting the evolution of both bond types with minimum amount invested over a 50 years period
    plt.plot(FV_short_list, label='short term bond')
    plt.plot(FV_long_list, label='long term bond')
    plt.xlabel('Years')
    plt.ylabel('Future value')
    plt.title('Investment evolution')
    plt.legend()
    plt.show()
