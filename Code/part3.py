import functions
from datetime import datetime

print("For 'Defensive' enter 0, \n"
      "For 'Aggressive' enter 1 \n"
      "For 'Mixed' enter 2 \n")

i = int(input("Please enter your investment type: "))
invest_type = ['Defensive', 'Aggressive', 'Mixed']

if i < len(invest_type):
    print("Your investment type is: " + invest_type[i])
else:
    print("Please enter a valid choice")
print("Please put Budget more than 5000 $\n")

amount = int(input("Please enter your budget: "))
if amount < 5000:
    print("Your budget has to be more than 5000")
else:
    print(amount)
    if i == 0:
        functions.defensive(amount)
    elif i == 1:
        st_date = (input('Enter start date(yyyy-mm-dd): '))
        start_date = datetime.strptime(st_date, "%Y-%m-%d")
        functions.aggressive(amount, start_date)
    else:
        st_date = (input('Enter start date(yyyy-mm-dd): '))
        start_date = datetime.strptime(st_date, "%Y-%m-%d")
        functions.mixed(amount,start_date)

