import random
from Part1 import short, long
import datetime as dt
import functions
from random import randrange
from datetime import timedelta

price_of_long_bond = 1000
price_of_short_bond = 250


def defense(amount):
    """
    This functions calculates the final amount after investing into long term and maximum short term bonds possible
    :param amount: amount to invest
    :return: final amount
    """
    a = 0.5 * amount
    c = 250
    if a >= 1000:
        no_of_long_bonds = a // price_of_long_bond

        """ Randomly select the number of bonds to buy within the range of maximum number of 
        bonds he can buy with the amount given"""
        max_long_bond = random.randint(0, int(no_of_long_bonds))
        buy_long = max_long_bond * price_of_long_bond

        new_amount = a - buy_long  # amount left after buying long bonds

        t = long(buy_long, 1)  # calling the subclass corresponding to long-term bond from part1 and creating a new object h
        FVL, compound_value_l = t.compute_price()

        div = (a+new_amount) % c  # remainder after extracting the short bond amount
        d = (a+new_amount - div)  # amount for short term bonds after adding the remaining amount and subtracting the remainder
        h = short(d,1)  # calling the subclass corresponding to short-term bond from part1 and creating a new object h
        FVS, compound_value_s = h.compute_price()
        final_amount = FVS + FVL + div # final amount is the sum of future value short, long and remainder
    else:
        div = amount % c  # remainder after extracting the short bond amount
        d = amount - div  # amount for short term bonds after subtracting the remainder
        max_short_bond = d // price_of_short_bond

        """ Randomly select the number of bonds to buy within the range of maximum number of 
             short bonds he can buy with the amount given"""
        no_of_short_bonds = random.randint(0, int(max_short_bond))
        buy_short = no_of_short_bonds * price_of_short_bond

        h = short(buy_short,1)  # calling the subclass corresponding to short-term bond from part1 and creating a new object h
        FVS, compound_value_s = h.compute_price()

        final_amount = FVS + div  # final amount is the sum of future value short in this case and remainder
    return final_amount


def aggress(amount, year):
    """
    This functions calculates the final amount after investing into the stocks possible
    :param amount: investment amount
    :param year: investment year
    :return: final value
    """
    start_dt = dt.datetime(year, 1, 4)
    end_dt = dt.datetime(year, 12, 31)
    start_date = random_date(start_dt, end_dt)
    final_date = dt.datetime(year, 12, 31)

    lst_stock = ['FDX', 'GOOGL', 'XOM', 'KO', 'NOK', 'MS', 'IBM']  # List of all the stocks
    stock_name = random.choice(lst_stock)
    final_price1, no_stocks2 = functions.stock_details(stock_name, final_date, amount)

    initial_price, no_stocks = functions.stock_details(stock_name, start_date, amount)

    lst_stock2 = lst_stock
    lst_stock2.remove(stock_name) # remove the already bought stock from the list

    remaining_amount = amount % initial_price

    initial_price_new = 0
    dict_newstocks = {}
    i = 0

    """
    loop until there is no remaining amount left to be invested
    """
    while initial_price_new < remaining_amount:
        if lst_stock2:
            stock_namenew = random.choice(lst_stock2)
            initial_price_new, no_stocks1 = functions.stock_details(stock_namenew, start_date,
                                                                    remaining_amount)
            final_price2, no_stocks4 = functions.stock_details(stock_namenew, final_date, initial_price_new)
            lst_stock2.remove(stock_namenew)
            dict_newstocks[i] = [no_stocks1, initial_price_new, final_price2, remaining_amount]
            if no_stocks1 == 0:
                initial_price_new = 0
            else:
                remaining_amount = remaining_amount % initial_price_new
                '''print("Total number of stocks is " + str(no_stocks1))
                print("The remaining amount after buying new stock " + str(round(remaining_amount, 2)))'''
        else:
            print("Not sufficient money to buy another stock")
        break

    profit = ((final_price1 - initial_price) * no_stocks) + remaining_amount
    profit_lstextra_stocks = [profit]
    if dict_newstocks.values():
        for values in dict_newstocks.values():
            profit1 = ((values[2] - values[1]) * values[0]) + values[3]
            profit_lstextra_stocks.append(profit1)
    total = sum(profit_lstextra_stocks)
    final_value = total + amount
    return final_value


def random_date(start, end):
    """
    This function will return a random datetime between two datetime
    objects.
    """
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = randrange(int_delta)
    return start + timedelta(seconds=random_second)
