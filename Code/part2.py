# to download data of stocks investpy and datetime for the input of date and alteration of days
import investpy
from datetime import datetime
from datetime import timedelta
from matplotlib import pyplot as plt

# To ask investor about the stock he is interested to invest in
print("For 'FDX' enter 0, \n"
      "For 'GOOGL' enter 1 \n"
      "For 'XOM' enter 2 \n"
      "For 'KO' enter 3 \n"
      "For 'NOK' enter 4 \n"
      "For 'MS' enter 5 \n"
      "For 'IBM' enter 6")

print("The term of your investment (start-date and end-date)\n"
      "must be between 09/01/2016 and 01/01/2021")

stock_name = int(input("Please enter the stock. "))
lst_stock = ['FDX','GOOGL', 'XOM', 'KO', 'NOK', 'MS', 'IBM']    # List of all the stocks

if stock_name < len(lst_stock):     # if he replies another number than the given he will get an error message
    amount = int(input("Please enter the investment amount: "))        # Amount of his investment
    my_string_start = str(input('Enter start date(yyyy-mm-dd): '))     # Date he wishes to buy the stock
    my_string_end = str(input('Enter end date(yyyy-mm-dd): '))      # Date he wishes to close his position
    start_date = datetime.strptime(my_string_start, "%Y-%m-%d")     # to remove hours
    end_date = datetime.strptime(my_string_end, "%Y-%m-%d")     # to remove hours
    stock = investpy.get_stock_historical_data(stock=lst_stock[stock_name],     # The data of the stocks from 9/1/2016 to 1/1/2021
                                               country='United States',         # function invespy requires internet connection
                                               from_date='09/01/2016',          # information download from investing.com
                                               to_date='01/01/2021')

    stock.reset_index(level=0, inplace=True)       # makes Date as column
    stock = stock[['Date', 'High']]     # Keeps the columns Date and High
    endNumber = end_date.weekday()      # Gives particular number to each end-day
    start_number1 = start_date.weekday()  # Gives particular number to each start-day

# To account for holidays and weekends, we correct the start date here
    if start_number1 == 6:
        dat_prev_start = datetime.strftime(start_date - timedelta(days=2), "%Y-%m-%d")  # go back two days if ends up on sunday
    elif start_number1 == 5:
        dat_prev_start = datetime.strftime(start_date - timedelta(days=1), "%Y-%m-%d")  # go back one day if ends up on saturday
    elif start_number1 in [0, 1, 2, 3] and start_date not in stock['Date']:
        dat_prev_start = datetime.strftime(start_date + timedelta(days=1), "%Y-%m-%d")
    elif start_number1 == 4 and start_date not in stock['Date']:               # checks if start date is contained in the dataset
        dat_prev_start = datetime.strftime(start_date - timedelta(days=1), "%Y-%m-%d")
    else:
        dat_prev_start = start_date
# if its saturday takes value of -1 day  and if sunday value of -2 day otherwise remains the same

# To account for holidays and weekends, we correct the end date here
    if endNumber == 6:
        dat_prev_end = datetime.strftime(end_date - timedelta(days=2), "%Y-%m-%d")
    elif endNumber == 5:
        dat_prev_end = datetime.strftime(end_date - timedelta(days=1), "%Y-%m-%d")
    elif endNumber in [0, 1, 2, 3] and end_date not in stock['Date']:
        dat_prev_end = datetime.strftime(end_date + timedelta(days=1), "%Y-%m-%d")
    elif endNumber == 4 and end_date not in stock['Date']:
        dat_prev_end = datetime.strftime(end_date - timedelta(days=1), "%Y-%m-%d")
    else:
        dat_prev_end = end_date

    # The price selected after the correction with days is set as the initial price for the day of his investment
    initial_prices = stock[stock['Date'] == dat_prev_start]  # check for all prices in the dataset corresponding to the corrected date
    initial_prices = initial_prices['High'].values[0]   # takes only first value in column high of the filtered dataset with relevant prices
    print("Price for " + str(start_date) + " is " + str(initial_prices))

    # The price selected after the correction with days is set as the final  price for the last day of his investment
    final_price = stock[stock['Date'] == dat_prev_end]
    final_price = final_price['High'].values[0]
    print("Price of " + lst_stock[stock_name] + " on the " + str(end_date) + " is " + str(final_price))

    # number of stocks he can buy is calculated by invested amount/stock price
    no_stock = int(amount / initial_prices)
    print(no_stock)

    # Final return on his investment is given by the returns formula
    roi = round((((final_price - initial_prices) * no_stock) / initial_prices), 3)
    print("Final value is " + str(roi))

else:
    print('Please enter the correct stock number')


# Plotting the stock prices


fig, STOCKS = plt.subplots(1, 1, facecolor='w', edgecolor='k')    # Creating a figure for the graph in dimension (1,1)

# To take each stock from the list and storing the date and high values

for x in lst_stock:
    stock_x = investpy.get_stock_historical_data(stock=x,
                                               country='United States',
                                               from_date='09/01/2016',
                                               to_date='01/01/2021')

    stock_x.reset_index(level=0, inplace=True)     # To make date as another column with new index
    stock_x = stock_x[['Date', 'High']]     # Only Date and High values from the stock data

    STOCKS.plot(stock_x['Date'], stock_x['High'], label=x)      # To plot the stock prices and label the curves


STOCKS.legend()
STOCKS.set_ylabel('Price')
STOCKS.set_xlabel('Date')
plt.show()

