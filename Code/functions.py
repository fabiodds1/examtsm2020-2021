from Part1 import short, long
import investpy
import random


def defensive(amount):  # Defensive investor

    n = int(input("Please enter the term of your investment in years. "))

    a = 0.5 * amount
    c = 250
    div = a % c   # computing remaining amount
    d = a - div     # to compute the corresponding amount allowing us to buy short term bonds given a price of 250

    h = short(d, n)  # calling the subclass corresponding to short-term bond from part1 and creating a new object h
    FVS, compound_value_s = h.compute_price()
    print("The future short term bond value is " + str(round(FVS, 2)) + " and the compound interest is " + str(
        round(compound_value_s, 2)))

    t = long(a, n)  # calling the subclass corresponding to long-term bond from part1 and creating a new object h
    FVL, compound_value_l = t.compute_price()
    print("The future long term bond value is " + str(round(FVL, 2)) + " and the compound interest is " + str(
        round(compound_value_l, 2)))


def aggressive(amount, start_date):

    lst_stock = ['FDX', 'GOOGL', 'XOM', 'KO', 'NOK', 'MS', 'IBM']  # List of all the stocks
    stock_name = random.choice(lst_stock)
    print(stock_name)

    initial_price, no_stocks = stock_details(stock_name, start_date, amount)
    print("Total number of stocks is " + str(no_stocks))

    remaining_amount = amount % initial_price
    print("The remaining amount after buying first stock: " + str(round(remaining_amount, 2)))

    lst_stock2 = lst_stock
    lst_stock2.remove(stock_name)  # removing first ticker from list of stocks
    print(lst_stock2)

    initial_price_new = 0
    while initial_price_new < remaining_amount:    # checking if budget is enough to purchase more stocks

        if lst_stock2:
            stock_namenew = random.choice(lst_stock2)
            initial_price_new, no_stocks1 = stock_details(stock_namenew, start_date, remaining_amount)
            # removing following ticker from list of stocks. Task repeated until no more money
            lst_stock2.remove(stock_namenew)  
            if no_stocks1 == 0:
                initial_price_new = 0
            else:
                remaining_amount = remaining_amount % initial_price_new   
                print(stock_namenew)
                print("Total number of stocks is " + str(no_stocks1))
                print("The remaining amount after buying new stock " + str(round(remaining_amount, 2)))
        else:
            print("Not sufficient money to buy another stock")
            break


def stock_details(stock_name, start_date, amt):
    stock = investpy.get_stock_historical_data(stock=stock_name,
                                              # The data of the stocks from 9/1/2016 to 1/1/2021
                                              country='United States',
                                              from_date='20/12/2015',
                                              to_date='10/1/2021')
    stock2 = stock
    stock2.reset_index(level=0, inplace=True)       # makes Date as column
    stock2 = stock2[['Date', 'High']]

    if start_date not in stock2['Date']:
        start_date = nearest(stock2['Date'], start_date)   # converting to the nearest date before
        stock.reset_index(level=0, inplace=True)  # makes Date as column
        stock = stock[['Date', 'High']]    # reshaping dataset
    else:
        start_date = start_date
        stock.reset_index(level=0, inplace=True)  # makes Date as column
        stock = stock[['Date', 'High']]   # reshaping dataset

    initial_prices = stock[stock['Date'] == start_date]
    initial_prices = initial_prices['High'].values[0]
    # print("Price for " + str(st_date) + " is " + str(initial_prices))

    # number of stocks he can buy is calculated by invested amount/stock pricenb
    no_stock = int(amt / initial_prices)

    return initial_prices, no_stock


def mixed(amount, start_date):
    bond = 0.25 * amount    # amount invested in bonds given the weights
    stock = 0.75 * amount   # amount invested in stocks given the weights
    defensive(bond)
    aggressive(stock,start_date)

# function to compute the nearest date before a given that in case the date doesn't exist on the dataset
# this function is an alternative to the method used before with datetime, weekday and timedelta
def nearest(items, pivot):   # items represent the price('HIGH') column and pivot our initial date
    return min(items, key=lambda x: abs(x - pivot))