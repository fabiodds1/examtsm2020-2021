class bond():

    def __init__(self, principal, maturity): # constructor function with class attributes, principal and maturity
        self.principal = principal
        self.maturity = maturity


# subclass of bond that inherits its attributes but has particular  characteristics of short term bond
class short(bond):
    def compute_price(self):  # this method computes future value and compounded interest given a interest rate of 1.5%
        rate = 0.015
        FV = self.principal * ((1 + rate) ** (self.maturity))
        compound_value = FV - self.principal

        return FV, compound_value


# subclass of bond that inherits its attributes but has particular characteristics of long term bond
class long(bond):
    def compute_price(self):  # this method computes future value and compounded interest given a interest rate of 3%
        rate = 0.03
        FV = self.principal * ((1 + rate) ** (self.maturity))
        compound_value = FV - self.principal

        return FV, compound_value
